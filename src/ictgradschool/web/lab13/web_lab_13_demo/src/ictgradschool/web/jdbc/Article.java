package ictgradschool.web.jdbc;

/**
 * Represents a single article in our database. Articles have an id, title, and content.
 */
public class Article {

    private Integer id;

    private String title;

    private String content;

    public Article() {
    }

    public Article(Integer id, String title, String content) {
        this.id = id;
        this.title = title;
        this.content = content;
    }

    public Article(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();

        builder.append("Article #" + id + ": " + title);
        builder.append(System.lineSeparator());
        builder.append("- " + content);

        return builder.toString();

    }
}
