package ictgradschool.web.lab13.ex2;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Exercise02 {
    public static void main(String[] args) {
        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("Welcome to the Film database!");
            //Anything that requires the use of the connection should be in here...
            int count = 0;
            while (count == 0) {
                System.out.println("Please select an option from the following: ");
                System.out.println("1.Information by Actor");
                System.out.println("2.Information by Movie");
                System.out.println("3.Information by Genre");
                System.out.println("4.Exit");
                String selectedOption = ictgradschool.web.jdbc.Keyboard.readInput("> ");
                //System.out.println(selectedOption);
                if (selectedOption.equals("1")) {
                    System.out.println("Please enter the name of the actor you wish to get information about, or press enter to return to the previous menu");
                    String enterActorName = ictgradschool.web.jdbc.Keyboard.readInput("> ");
                    try (PreparedStatement preparedStatement = conn.prepareStatement("SELECT film_title FROM pfilms_film, pfilms_actor WHERE pfilms_actor.actor_fname OR pfilms_actor.actor_lname LIKE '%" + enterActorName + "%';")) {

                        try (ResultSet resultSet = preparedStatement.executeQuery()) {
                            while (resultSet.next()) {
                                System.out.println(resultSet.getString(1));
                                count++;
                            }
                        }


                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
